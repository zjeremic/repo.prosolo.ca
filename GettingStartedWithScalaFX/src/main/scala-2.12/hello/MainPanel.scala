package hello



import scalafx.Includes.handle
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control.{Alert, Button, Tooltip}
import scalafx.scene.layout.VBox
import scalafx.scene.text.Text
import scalafx.stage.Stage

/**
  * Created by zoran on 21/01/17.
  */
/**
  * zoran 21/01/17
  */
class MainPanel(stage:Stage, user:User) {
  stage.scene=scene1
  stage.width=800
  stage.height=600
  stage.title="Main application"
  private def scene1:Scene={
    val scene1 = new Scene {
      content= new VBox{
        padding=Insets(20)
        children=Seq(
          new Text{
            text="Dialog example"
            style = "-fx-font-size: 12pt"
          },
          createDialogButton,
          new Text{
            text="Switch panel"
            style="-fx-font-size: 12pt"
          },
          createSwitchButton(2)

        )
      }
    }
    scene1
  }
  private def scene2:Scene={
    val scene2 = new Scene {

      content= new VBox{
        padding=Insets(20)
        children=Seq(

          new Text{
            text="Switch panel"
            style="-fx-font-size: 12pt"
          },
          createSwitchButton(1)

        )
      }
    }
    scene2
  }

  private def createDialogButton: Button ={
    new Button {
      id = "DialogButton"
      text="Open Dialog"
      tooltip = Tooltip("New Document... Ctrl+N")
      onAction = handle {showAlert(AlertType.Information,"Some information content","My Personal Header", "Information Dialog")}
    }
  }
  private def createSwitchButton(sceneId:Int): Button ={
    new Button {
      id = "SwitchButton"
      text="Next Panel"
      tooltip = Tooltip("Next Panel")
      onAction = handle {switchScene(sceneId)}
    }
  }
  private def showAlert(alertType:AlertType, message: String, header:String, dialogTitle: String):Unit={
    val alert=new Alert(alertType){
      headerText=header
      contentText=message
      title=dialogTitle
      resizable=true
    }
    alert.showAndWait()
  }

  private def switchScene(sceneId:Int):Unit={

    println("Switching to scene:"+sceneId)
    if (sceneId==1) stage.scene=scene1 else stage.scene=scene2

  }

}
