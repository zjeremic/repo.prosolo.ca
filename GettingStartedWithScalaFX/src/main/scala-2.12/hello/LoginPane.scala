package hello




import javafx.scene.paint.{ImagePattern, Paint}

import scalafx.Includes.handle
import scalafx.geometry.{HPos, Insets, Pos, VPos}
import scalafx.scene.Scene
import scalafx.scene.control.{Button, Label, PasswordField, TextField}
import scalafx.scene.image.{Image, ImageView}
import scalafx.scene.layout._
import scalafx.scene.layout.{BackgroundPosition => sfxBackgroundPosition}
import scalafx.scene.paint.Color
import scalafx.scene.shape.Rectangle
import scalafx.stage.Stage

/**
  * Created by zoran on 21/01/17.
  */
/**
  * zoran 21/01/17
  */
class LoginPane(stage:Stage) {
val border=new BorderPane()

  stage.title="Login"
  stage.width=400
  stage.height=400
  stage.centerOnScreen
  stage.scene=new Scene(border)
  createLogin


  private def createLogin {
    val textUsername = new TextField {
      promptText = "Username"
    }
    val passwordField = new PasswordField {
      promptText = "Password"
    }

    val loginButton = new Button {
      text = "Sign in"
      disable = true
      onMouseClicked = handle {
        if (verifyUser(textUsername.text.value, passwordField.text.value)) {
          toMainPanel(new User(textUsername.text.value))
        } else {
          println("WRONG USER")
        }

      }
    }
    textUsername.text.onChange {
      (_, _, newValue) =>
        loginButton.disable = newValue.trim().isEmpty
    }
    val logoLabel=new Label{
      id = "Logo"
      graphic = logo
      margin = Insets(10,10,10,10)
    }
    val stack = new StackPane {
      background = createBackground(Color.White, 0, 0)
      padding = Insets(10)
      children.add(logoLabel)
      //children.add(createImage("utalogo.jpg",125,125))
    }

    val vBox=new VBox{
      padding = Insets(30)

      spacing = 8
      children.add(new Label("Username:"))
      children.add(textUsername)
      children.add(new Label("Password:"))
      children.add(passwordField)
      children.add(loginButton)
    }
    val stackLogin=new StackPane{

      background = createBackground(Color.White, 0, 0)
      padding=Insets(10)
      children.add(vBox)
    }
    border.top=stack
    border.center=stackLogin

  }

  private def verifyUser(user:String, pass:String):Boolean={

    if (user.equals("")||pass.equals("")){
      false
    }else{
      if(user.equals("admin")&&(pass.equals("admin"))){

        true
      }else false
    }
  }

  private def toMainPanel(user:User)=new MainPanel(stage,user)

  def createBackground(color: Color, cornerSize: Int, insets: Int): Background = {
    val backgroundFill : Array[BackgroundFill] = Array(new BackgroundFill(color, new CornerRadii(cornerSize), Insets(insets)))
    val background = new Background(backgroundFill)
    background
  }
  def createImage(imageName : String, width : Double, height : Double):Image={
    val img = new Image("file:src/main/resources/images/" + imageName, width, height, true, true)
    img
  }
  def createImagePattern(imageName : String, width : Double, height : Double): ImagePattern = {
    val img = new Image("file:src/main/resources/images/" + imageName, width, height, true, true)
    val imgPattern = new ImagePattern(img)
    imgPattern
  }
  def logo(): ImageView = {
    val image = new Image("file:src/main/resources/images/utalogo.jpg", 125, 125, true, true)
    val imgview = new ImageView(image)
    imgview
  }
  //  loginScene

 // }



}
